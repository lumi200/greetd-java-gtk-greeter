package at.lumi200;

import at.lumi200.libgreetd.AuthResponse;
import at.lumi200.libgreetd.Greetd;
import at.lumi200.libgreetd.enums.MessageType;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

public class LoginHelper {
    private static final Greetd greetd;

    static {
        try {
            greetd = new Greetd();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Greetd getGreetd() {
        return greetd;
    }

    public AuthResponse createSession(String username, String password) {

        AuthResponse authResponse;
        try {
            authResponse = greetd.createSession(username);

            if (authResponse.getMessageType().equals(MessageType.AUTH_MESSAGE)) {
                authResponse = greetd.authenticate(password);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return authResponse;
    }

}
