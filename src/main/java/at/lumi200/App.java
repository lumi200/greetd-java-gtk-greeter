package at.lumi200;

import at.lumi200.libgreetd.AuthResponse;
import at.lumi200.libgreetd.Greetd;
import ch.bailu.gtk.gio.ApplicationFlags;
import ch.bailu.gtk.gtk.*;
import ch.bailu.gtk.type.Strs;
import ch.bailu.gtk.type.exception.AllocationError;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

/**
 * Hello world!
 */
public class App {
	public static void main(String[] args) {

		Application app = new Application("at.lumi200.greetd-java-gtk-greeter", ApplicationFlags.FLAGS_NONE);
		app.onActivate(() -> {

			Builder b = new Builder();
			Greetd greetd;

			try {
				//Setup GreetD
				LoginHelper loginHelper = new LoginHelper();
				greetd = LoginHelper.getGreetd();

				//Setup GTK Window
				b.addFromFile("src/main/resources/greeter.ui");
				Window window = new Window(b.getObject("window").cast());
				window.setApplication(app);
				Entry txtUsername = new Entry(b.getObject("txtUsername").cast());
				//TODO: Replace with password entry
				Entry txtPassword = new Entry(b.getObject("txtPassword").cast());
				Button btnLogin = new Button(b.getObject("btnLogin").cast());
				InfoBar infoBar = new InfoBar(b.getObject("infoBar").cast());
				Label authMsg = new Label(b.getObject("authMsg").cast());
				Entry authEntry = new Entry(b.getObject("authEntry").cast());

				btnLogin.onClicked(() -> {
					btnLogin.setSensitive(false);
					infoBar.setRevealed(false);
					txtUsername.setCanFocus(false);
					txtPassword.setCanFocus(false);
					AuthResponse authResponse = loginHelper.createSession(
							String.valueOf(txtUsername.getBuffer().getText()),
							String.valueOf(txtPassword.getBuffer().getText()));

					switch (authResponse.getMessageType()) {
						case SUCCESS -> {
							infoBar.setRevealed(true);
							authMsg.setLabel("Startup Command:");
						}
						case ERROR -> {
							authEntry.hide();
							authMsg.setLabel("Wrong Password");
							infoBar.setMessageType(MessageType.ERROR);
							infoBar.setRevealed(true);
							btnLogin.setSensitive(true);
							txtUsername.setCanFocus(true);
							txtPassword.setCanFocus(true);
						}
						case AUTH_MESSAGE -> {
							authMsg.setLabel(authResponse.getAuthMessage());
							infoBar.setRevealed(true);
						}
						default -> {btnLogin.setLabel("UNKNOWN MESSAGE");}
					}

				});

				authEntry.onActivate(() -> {
					//TODO: Find better solution to switch between login and second auth
					if (authMsg.getLabel().toString().equals("Startup Command:")) {
						try {
							AuthResponse authResponse = greetd.startSession(
									String.valueOf(authEntry.getBuffer().getText()).split(" "),
									new String[]{""});

							if (authResponse.getMessageType().equals(at.lumi200.libgreetd.enums.MessageType.SUCCESS))
								System.exit(0);
						} catch (JsonProcessingException e) {
							throw new RuntimeException(e);
						}
					} else {
						try {
							AuthResponse authResponse = greetd.authenticate(String.valueOf(authEntry.getBuffer().getText()));

							while (authResponse.getMessageType().equals(at.lumi200.libgreetd.enums.MessageType.AUTH_MESSAGE)) {
								authMsg.setLabel(authResponse.getAuthMessage());
								authResponse = greetd.authenticate(String.valueOf(authEntry.getBuffer().getText()));
							}
							authMsg.setLabel("Startup Command:");

						} catch (JsonProcessingException e) {
							throw new RuntimeException(e);
						}
					}
				});


				window.show();


			} catch (AllocationError | IOException e) {
				throw new RuntimeException(e);
			}
		});

		app.run(args.length, new Strs(args));


	}

}
